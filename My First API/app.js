var express = require("express");
var app = express();
app.get("/hello", (req, res, next) => {
    const message = req.query.message || 'world'
    res.json([{"response": "Hello $[message]"}]);
   });
app.listen(3000, () => {
 console.log("Server running on port 3000");
});
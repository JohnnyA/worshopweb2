const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express();

const db = require('./config/db').database;
 //conexion a la base de dato

 mongoose.connect(db, {useNewUrlParser: true})
         .then(() =>{
             console.log('Base de datos conectada correctamente')
         })
         .catch((err) =>{
             console.log('Base de datos no conectada', err)
         });


const port = process.env.PORT || 5000;

app.use(cors());

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('<h1>hola</h1>');
});

app.listen(port, () => {
    console.log('servidor iniciado en el puerto', port);
});